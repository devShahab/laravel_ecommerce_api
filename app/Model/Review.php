<?php

namespace App\Model;

use App\Model\Product;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'product_id', 'customer', 'star', 'review',
    ];

    public function product()
	{
		// 2nd param -> foriegn column name i.e Review has product_id as reference from Product
		// 3rd param -> refered table column name i.e Product primary key "id" is used as reference by 
		//              Review so we will use Product table column 'id'
	    return $this->belongsTo(Product::class, 'product_id', 'id');
	}

}
