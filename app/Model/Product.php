<?php

namespace App\Model;

use App\Model\Review;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'detail', 'price', 'stock', 'discount',
    ];

    public function reviews()
    {
    	// 1st param -> model class i.e Review::class
    	// 2nd param -> reference column name i.e Review has reference product_id from Product::class
    	// 3rd param -> primary key of model i.e Product::class primary key that is used as reference by
    	//              Review::class
    	return $this->hasMany(Review::class, 'product_id', 'id');
    }
}
