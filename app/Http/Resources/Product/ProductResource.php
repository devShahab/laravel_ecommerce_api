<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->user_id,
            'name' => $this->name,
            'description' => $this->detail,
            'price' => $this->price,
            'discount' => $this->discount,
            'stock' => $this->stock > 0 ? $this->stock : 'Empty stock', 
            'totalPrice' => round((1 - ($this->discount / 100)) * $this->price, 2),
            'rating' => $this->reviews()->count() > 0 ? round($this->reviews()->sum('star')/$this->reviews()->count()) : 'No rating yet',
            'href' => [
                'reviews' => route('reviews.index', $this->id)
            ],
            
        ];
    }
}
