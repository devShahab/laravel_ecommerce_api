<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTrait
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function apiException($request, Exception $e)
    {
        if($this->isModel($e)){
            return $this->modelErrorResponse();
        }
        else if($this->isHttp($e)){
            return $this->httpErrorResponse();
        }else{
            return parent::render($request, $e);
        }
    }


    public function isModel(Exception $e){
        return $e instanceof ModelNotFoundException;
    }

    public function isHttp(Exception $e){
        return $e instanceof NotFoundHttpException;
    }

    public function modelErrorResponse(){
        return response([
                    'error' => 'Model not found.'
                ], Response::HTTP_NOT_FOUND);
    }

    public function httpErrorResponse(){
        return response([
                    'error' => 'Incorrect route.'
                ], Response::HTTP_NOT_FOUND);
    }

}