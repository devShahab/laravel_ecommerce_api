<?php

namespace App\Exceptions;
use Symfony\Component\HttpFoundation\Response;

use Exception;

class ProductNotBelongsToUserException extends Exception
{
    public function render()
    {
    	return response([
    		'error'=> 'Product not belongs to you.'
    	], Response::HTTP_NOT_FOUND);
    }
}
