<?php

namespace App\Providers;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // disable json response wrap in data attribute
        Resource::withoutWrapping();

        // to set the string lenght
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // to set the faker language to english
        $this->app->singleton(FakerGenerator::class, function () {
          return FakerFactory::create('en_US');
        });
    }
}
