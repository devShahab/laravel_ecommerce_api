<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Model\Product::class, function (Faker $faker) {
    return [
    	'user_id' => function () {
            return User::all()->random();
        },
        'name' => $faker->word,
        'detail' => $faker->paragraph,
        'price' => $faker->randomFloat(2, 1, 100),
        'stock' => $faker->randomDigit,
        'discount' => $faker->randomFloat(2, 0,50),
    ];
});
